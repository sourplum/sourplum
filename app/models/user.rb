class User < ActiveRecord::Base
  # Use friendly_id on Users
  extend FriendlyId
  friendly_id :friendify, use: :slugged

  # necessary to override friendly_id reserved words
  def friendify
    if username.downcase == 'admin'
      "user-#{username}"
    else
      "#{username}"
    end
  end

  acts_as_messageable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :username, presence: true,
            length: {
                in: 4..15
            },
            uniqueness: {
                case_sensitive: false
            },
            format: {
                with: /\A[a-zA-Z0-9]*\z/,
                message: 'can only contain letters and digits'
            }

  validates :email, presence: true,
            format: {
                with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
            }
  validates :password, presence: true, on: :create
  validates :password_confirmation, presence: true, on: :create

  has_many(
      :owned_subs,
      foreign_key: :moderator_id,
      primary_key: :id,
      class_name: 'Sub'
  )

  has_many(
      :posts,
      foreign_key: :submitter_id,
      primary_key: :id,
      class_name: 'Post'
  )

  has_many(
      :comments,
      foreign_key: :submitter_id,
      primary_key: :id,
      class_name: 'Comment'
  )

  # Virtual attribute for authenticating by either username or email
  # This is in addition to a real persisted field like 'username'
  # attr_accessor :login

  # validate :validate_username

  # def validate_username
  #   if User.where(email: username).exists?
  #     errors.add(:username, :invalid)
  #   end
  # end
  #
  # def self.find_for_database_authentication(warden_conditions)
  #   conditions = warden_conditions.dup
  #   if login = conditions.delete(:login)
  #     where(conditions.to_hash).where(['lower(username) = :value OR lower(email) = :value', {:value => login.downcase}]).first
  #   else
  #     where(conditions.to_hash).first
  #   end
  # end

  def name
    email
  end

  def mailboxer_email(object)
    email
  end

end
