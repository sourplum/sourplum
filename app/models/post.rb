class Post < ActiveRecord::Base
  validates :title, :sub_id, :submitter_id, presence: true

  belongs_to(
      :sub,
      foreign_key:  :sub_id,
      primary_key:  :id,
      class_name: 'Sub'
  )

  belongs_to(
      :submitter,
      foreign_key:  :submitter_id,
      primary_key:  :id,
      class_name: 'User'
  )

  has_many(
      :comments,
      foreign_key:  :post_id,
      primary_key:  :id,
      class_name: 'Comment',
      dependent: :destroy
  )
end
