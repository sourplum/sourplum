class DisastersController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :must_be_moderator, only: [:edit, :update, :destroy]

  # GET /subs
  # GET /subs.json
  def index
    @subs = Sub.with_disaster.all
  end

  # GET /subs/1
  # GET /subs/1.json
  def show
    @sub = Sub.includes(:moderator).friendly.find(params[:id])
  end

  private

  def must_be_moderator
    sub = Sub.friendly.find(params[:id])
    unless current_user == sub.moderator
      render json: {msg: "You aren't the moderator of this sub."}, status: 401
    end
  end
end
