class AddColumnDisasterToSub < ActiveRecord::Migration
  def change
    add_column :subs, :disaster, :boolean, default: false
  end
end
